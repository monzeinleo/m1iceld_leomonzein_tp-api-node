# m1iceld_LeoMONZEIN_TP-API-Node

## GET method

![GET route](./images/GET.png)

<br>

## POST method

![POST route](./images/POST.png)

<br>

## PUT method

![PUT route](./images/PUT.png)

<br>

## DELETE method

![DELETE route](./images/DELETE.png)


## Helmet

Helmet is used to help secure HTTP headers returned by Express apps by hiding sensitive informations that would be otherwise disclosed.