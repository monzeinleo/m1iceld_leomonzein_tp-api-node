#!usr/bin/env node

'use strict'

const fs            = require('fs');
const express       = require('express');
const uuid          = require('uuid');
const bodyParser    = require('body-parser')
const helmet        = require('helmet')
const APP           = express();
const PORT          = 3000;

APP.set('view engine', 'pug');
APP.use(bodyParser.urlencoded({ extended: false }));
APP.use(bodyParser.json());
APP.use(helmet());

// Render a response
function renderResponse(res, code, message) {
    res.statusCode = code;
    res.setHeader('Content-type', 'text/html');
    res.render('response', { code: code, message: message });
}

// GET all cities
APP.get('/cities', (req, res) => {
    if (fs.existsSync("cities.json")) {
        fs.readFile("cities.json", 'utf8', (err,data) => {
            if (err) {
                renderResponse(res, 500, err );
            } else {
                var cityList = JSON.parse(data);
                cityList = cityList.cities;
                res.statusCode = 200;
                res.setHeader('Content-type', 'text/html');
                res.render('cities', { cities : cityList })
            }
        })
    } else {
        renderResponse(res, 404, "File 'cities.JSON' not found");
    };
});

// POST one city
APP.post('/city', (req, res) => {
    var alreadyExists = false;
    var cityName = req.body && req.body.name;
    if (!cityName) {
        renderResponse(res, 500, "You must specify a city name");
        return;
    }
    if (fs.existsSync("cities.json")) {
        fs.readFile("cities.json", 'utf8', (err,data) => {
            if (err) {
                renderResponse(res, 500, err );
            } else {
                var json = JSON.parse(data);
                for (var city of json.cities) {
                    if (city.name == cityName) {
                        alreadyExists = true;
                    }
                }
                if (alreadyExists) {
                    renderResponse(res, 500, `${cityName} already exists !`);
                } else {
                    json.cities.push({ "id" : uuid.v4(), "name" : cityName});
                    fs.writeFileSync("cities.json", JSON.stringify(json));
                    res.statusCode = 200;
                    res.setHeader('Content-type', 'text/html');
                    res.render('cities', { cities : json.cities })
                }
            }
        })
    } else {
        renderResponse(res, 404, "File 'cities.JSON' not found");
    };
})

// PUT one city
APP.put('/city/:id', (req, res) => {
    var alreadyExists = false;
    var cityId = req.body && req.body.id;
    if (!cityId) {
        renderResponse(res, 500, "You must specify a city id");
        return;
    }
    var cityName = req.body && req.body.name;
    if (!cityName) {
        renderResponse(res, 500, "You must specify a new city name");
        return;
    }
    if (fs.existsSync("cities.json")) {
        fs.readFile("cities.json", 'utf8', (err,data) => {
            if (err) {
                renderResponse(res, 500, err );
            } else {
                var json = JSON.parse(data);
                for (var city of json.cities) {
                    if (city.id == cityId) {
                        alreadyExists = true;
                        city.name = cityName;
                        fs.writeFileSync("cities.json", JSON.stringify(json));
                        res.statusCode = 200;
                        res.setHeader('Content-type', 'text/html');
                        res.render('cities', { cities : json.cities })
                        return;
                    }
                }
                if (!alreadyExists) {
                    renderResponse(res, 500, `${cityId} doesn't exists !`);
                }
            }
        })
    } else {
        renderResponse(res, 404, "File 'cities.JSON' not found");
    };
})

// DELETE one city
APP.delete('/city/:id', (req, res) => {
    var alreadyExists = false;
    var cityId = req.body && req.body.id;
    if (!cityId) {
        renderResponse(res, 500, "You must specify a city id");
        return;
    }
    if (fs.existsSync("cities.json")) {
        fs.readFile("cities.json", 'utf8', (err,data) => {
            if (err) {
                renderResponse(res, 500, err );
            } else {
                var newCityList = [];
                var json = JSON.parse(data);
                for (var city of json.cities) {
                    if (city.id == cityId) {
                        alreadyExists = true;
                    } else {
                        newCityList.push(city);
                    }
                }
                if (alreadyExists) {
                    json.cities = newCityList;
                    fs.writeFileSync("cities.json", JSON.stringify(json));
                    res.statusCode = 200;
                    res.setHeader('Content-type', 'text/html');
                    res.render('cities', { cities : json.cities })
                } else {
                    renderResponse(res, 500, `${cityId} doesn't exists !`);
                }
            }
        })
    } else {
        renderResponse(res, 404, "File 'cities.JSON' not found");
    };
})

APP.listen(PORT, () => console.log(`Server listening on port ${PORT}...`));

